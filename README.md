Jeu en réalité virtuelle pour casques HTC vive

Le joueur se retrouve dans une pleine avec des monstres et des réservoirs posés sur le sol.
Les monstres s'attaquent aux arbres en les brûlant.

Le but du joueur est d'exterminer les monstres avec le pistolet avant que ceux-ci ne détruisent la forêt.