﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;
using Valve.VR.InteractionSystem;
using UnityEngine.AI;
using System;

public class Enemy : MonoBehaviour
{
    private Tree[] m_Trees = null;
    private Player m_Player = null;
    private NavMeshAgent m_NavAgent = null;
    private bool m_IsAlive = true;
    private GameObject m_Objective = null;
    [SerializeField]
    private float m_LifePoint = 100f;
    [SerializeField]
    private GameObject m_ParticleSystem = null;
    [SerializeField]
    private GameObject m_Mesh = null;


    private void Awake()
    {
        EventManager.Instance.AddListener<EnemyTouchedEvent>(OnEnemyTouched);
        EventManager.Instance.AddListener<TreeDestroyedEvent>(OnTreeDestroyed);
        m_Player = (Player)FindObjectOfType(typeof(Player));
        m_NavAgent = GetComponent<NavMeshAgent>();
    }

    private void OnTreeDestroyed(TreeDestroyedEvent e)
    {
        m_Trees = (Tree[])FindObjectsOfType(typeof(Tree));
    }

    private void OnEnemyTouched(EnemyTouchedEvent e)
    {
        if (gameObject.GetInstanceID() == e.enemy.GetInstanceID())
        {
            m_LifePoint -= 5f;
            transform.localScale = Vector3.one * Mathf.Lerp(.4f, 1f, ((m_LifePoint + 20) * 100 / 120) / 100f);
            if (m_LifePoint <= 0f)
            {
                StartCoroutine(DestroySelf());
            }
        }
    }

    private IEnumerator DestroySelf()
    {
        yield return null;
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<EnemyTouchedEvent>(OnEnemyTouched);
        EventManager.Instance.RemoveListener<TreeDestroyedEvent>(OnTreeDestroyed);
    }

    private IEnumerator Start()
    {
        while (m_IsAlive)
        {
            GetNearestObjective();
            if (Vector3.Distance(transform.position, m_Player.transform.position) > 20f)
            {
                m_NavAgent.SetDestination(transform.position);
            }
            else if (Vector3.Distance(transform.position, m_Objective.transform.position) <= 1.5f)
            {
                m_NavAgent.SetDestination(m_Objective.transform.position);
            }
            else
            {
                m_NavAgent.SetDestination(m_Objective.transform.position);
            }
            if (Vector3.Distance(transform.position, m_Player.transform.position) <= 1.5f)
            {
                EventManager.Instance.Raise(new TouchPlayerEvent() { enemy = gameObject });
            }
            yield return null;
        }
    }

    private void GetNearestObjective()
    {
        float actualDistance;
        float newDistance;
        m_Trees = (Tree[])FindObjectsOfType(typeof(Tree));
        GameObject nearest = m_Player.gameObject;
        foreach (Tree tree in m_Trees)
        {
            actualDistance = Vector3.Distance(transform.position, nearest.transform.position);
            newDistance = Vector3.Distance(transform.position, tree.transform.position);
            if (newDistance < actualDistance)
            {
                nearest = tree.gameObject;
            }
        }
        m_Objective = nearest;
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.GetComponent<Tree>())
        {
            EventManager.Instance.Raise(new TouchTreeEvent()
            {
                enemy = gameObject,
                tree = other.gameObject
            });
        }
    }
    private void Update()
    {
        if (Vector3.Distance(transform.position, m_Player.transform.position) > 30f)
        {
            m_ParticleSystem.SetActive(false);
            m_Mesh.SetActive(false);
        }
        else
        {
            m_ParticleSystem.SetActive(true);
            m_Mesh.SetActive(true);
        }
    }
}
