﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class GunShot : MonoBehaviour
{

    public AudioClip audioClip;
    public AudioSource audioSource;

    private bool m_IsGrabPinchDown = false;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (SteamVR_Actions._default.GrabPinch.GetStateDown(SteamVR_Input_Sources.RightHand)) { 
            m_IsGrabPinchDown = true;
            audioSource.PlayOneShot(audioClip, .7F);
            animator.SetBool("isShooting", true);
        }
        else
        {
            animator.SetBool("isShooting", false);
        }
        if (SteamVR_Actions._default.GrabPinch.GetStateUp(SteamVR_Input_Sources.RightHand))
            m_IsGrabPinchDown = false;
    }
}
