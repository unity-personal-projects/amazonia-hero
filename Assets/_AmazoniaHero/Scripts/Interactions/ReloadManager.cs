﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadManager : MonoBehaviour
{
    [SerializeField]
    private Ammo[] m_AmmoTanks = new Ammo[3];
    [SerializeField]
    private Transform[] m_Emplacements = new Transform[3];
    [SerializeField]
    private Transform m_BodyCollider = null;
    [SerializeField]
    private Transform m_Camera = null;
    bool canExit = true;
    private void Update()
    {
        transform.position = m_BodyCollider.position;
        transform.rotation = m_Camera.rotation;
    }
    private void OnCollisionEnter(Collision other)
    {
        Ammo m_Ammo = other.gameObject.GetComponent<Ammo>();
        if (m_Ammo && !HasEmplacementParent(m_Ammo))
        {
            bool alreadyPlaced = false;
            Debug.Log(m_Ammo.transform.name);
            foreach (Transform item in m_Emplacements)
            {
                Debug.Log("Child count " + item.name + " : " + item.childCount);
                if (item.childCount == 0 && !alreadyPlaced)
                {
                    StartCoroutine(PlaceAmmo(m_Ammo, item));
                    Debug.Log("SUCCESS Child count " + item.name + " : " + item.childCount);
                    alreadyPlaced = true;
                }
            }
        }
    }

    IEnumerator PlaceAmmo(Ammo m_Ammo, Transform item)
    {
        canExit = false;
        m_Ammo.gameObject.layer = LayerMask.NameToLayer("ReloadEmplacement");
        yield return null;
        m_Ammo.transform.SetParent(item);
        Rigidbody ammoRigidbody = m_Ammo.GetComponent<Rigidbody>();
        ammoRigidbody.isKinematic = true;
        ammoRigidbody.velocity = Vector3.zero;
        m_Ammo.transform.localPosition = Vector3.zero;
        m_Ammo.transform.localRotation = Quaternion.identity;
        m_Ammo.gameObject.layer = LayerMask.NameToLayer("Default");
        yield return null;
        canExit = true;
    }

    private void OnCollisionExit(Collision other)
    {
        Debug.Log("Exit " + other.transform.name);
        Ammo m_Ammo = other.transform.GetComponent<Ammo>();
        if (m_Ammo && canExit)
        {
            Debug.Log("Collision Exit " + m_Ammo.name);
            m_Ammo.GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    bool HasEmplacementParent(Ammo ammo)
    {
        if (ammo.transform.parent != null)
        {
            int ammoParentID = ammo.transform.parent.GetInstanceID();
            foreach (Transform item in m_Emplacements)
            {
                if (item.GetInstanceID() == ammoParentID)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
