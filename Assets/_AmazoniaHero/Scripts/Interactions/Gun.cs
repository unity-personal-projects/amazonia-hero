﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using SDD.Events;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{
    [SerializeField]
    private float m_LineLength = 15f;
    private GameObject m_ArcSpawnPoint = null;
    private LineRenderer m_ArcLineRenderer = null;
    private bool m_DisplayArc = false;
    private bool m_IsGrabPinchDown = false;
    private float m_WaterTank = 100f;
    private Image m_WaterImageBar = null;
    [SerializeField]
    private MeshRenderer m_BombonneRenderer = null;
    private Animator m_Animator = null;
    private AudioSource m_AudioSource = null;
    [SerializeField]
    private AudioClip m_FireWaterClip = null;
    private Coroutine m_StopShooting = null;
    [SerializeField]
    private GameObject m_WaterJet = null;

    private void Awake()
    {
        m_Animator = GetComponentInChildren<Animator>();
        m_AudioSource = GetComponent<AudioSource>();
        m_WaterImageBar = GetComponentInChildren<WaterBarSelector>().GetComponent<Image>();
        m_ArcLineRenderer = GetComponentInChildren<LineRenderer>();
        m_ArcSpawnPoint = m_ArcLineRenderer.gameObject;
        m_ArcLineRenderer.enabled = m_DisplayArc;
    }

    void Update()
    {
        float percentageWater = m_WaterTank / 100f;
        m_WaterImageBar.fillAmount = percentageWater;

        if (SteamVR_Actions._default.GrabPinch.GetStateDown(SteamVR_Input_Sources.RightHand))
        {
            m_IsGrabPinchDown = true;
        }
        if (SteamVR_Actions._default.GrabPinch.GetStateUp(SteamVR_Input_Sources.RightHand))
        {
            m_IsGrabPinchDown = false;
        }
        if (m_IsGrabPinchDown)
        {
            if (m_WaterTank > 0)
            {
                if (!m_AudioSource.isPlaying)
                {
                    if (m_StopShooting != null)
                    {
                        StopCoroutine(m_StopShooting);
                        m_StopShooting = null;
                    }
                    m_AudioSource.volume = 1f;
                    m_AudioSource.clip = m_FireWaterClip;
                    m_AudioSource.Play();
                    m_WaterJet.SetActive(true);
                }
                m_Animator.Play("Base_Position_Gun");
                RaycastHit hit;
                m_DisplayArc = true;
                if (Physics.Raycast(m_ArcSpawnPoint.transform.position, m_ArcSpawnPoint.transform.forward, out hit, m_LineLength))
                {
                    if (hit.transform != null)
                    {
                        if (hit.transform.GetComponent<Enemy>())
                        {
                            EventManager.Instance.Raise(new EnemyTouchedEvent()
                            {
                                enemy = hit.collider.gameObject
                            });
                            m_ArcLineRenderer.material.color = Color.green;
                        }
                        else if (hit.transform.GetComponent<Tree>())
                        {
                            EventManager.Instance.Raise(new PlayerTouchTreeEvent()
                            {
                                tree = hit.collider.gameObject
                            });
                        }
                        else
                        {
                            m_ArcLineRenderer.material.color = Color.yellow;
                        }
                    }
                    else
                    {
                        m_ArcLineRenderer.material.color = Color.red;
                    }
                }
                else
                {
                    m_ArcLineRenderer.material.color = Color.red;
                }
                m_ArcLineRenderer.SetPosition(0, m_ArcSpawnPoint.transform.position);
                m_ArcLineRenderer.SetPosition(1, m_ArcSpawnPoint.transform.position + m_ArcSpawnPoint.transform.forward * m_LineLength);
            }
            else
            {
                m_StopShooting = StartCoroutine(StopShootingEffects());
                m_DisplayArc = false;
                m_WaterTank = 0f;
                m_BombonneRenderer.enabled = true;
            }
            m_WaterTank -= .25f;
        }
        else
        {
            m_StopShooting = StartCoroutine(StopShootingEffects());
            m_DisplayArc = false;
        }
        m_ArcLineRenderer.enabled = m_DisplayArc;
    }

    IEnumerator StopShootingEffects()
    {
        if (m_Animator.GetCurrentAnimatorClipInfo(0).Length > 0 && m_Animator.GetCurrentAnimatorClipInfo(0)[0].clip.name == "Base_Position_Gun")
        {
            m_Animator.Play("Shooting_Position");
            float tempo = 0f;
            m_WaterJet.SetActive(false);
            while (tempo < 1f)
            {

                m_AudioSource.volume = 1f - tempo;
                yield return null;
                tempo += Time.deltaTime;
            }
            m_AudioSource.Pause();
            m_AudioSource.clip = null;
        }
    }

    private void OnCollisionStay(Collision other)
    {
        Ammo ammo = other.gameObject.GetComponentInChildren<Ammo>();
        if (ammo && m_WaterTank < 100f)
        {
            m_BombonneRenderer.enabled = true;
            m_WaterTank = 100f;
            Destroy(ammo.gameObject);
        }
    }
}
