﻿using System;
using System.Collections;
using System.Collections.Generic;
using SDD.Events;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerAmazonia : MonoBehaviour
{
    [SerializeField]
    private float m_LifePoint = 100f;
    [SerializeField]
    private string m_NameSceneMenu = null;
    private void Awake()
    {
        EventManager.Instance.AddListener<TouchPlayerEvent>(OnTouchPlayer);
    }

    private void OnTouchPlayer(TouchPlayerEvent e)
    {
        m_LifePoint -= .5f;
        if (m_LifePoint < 0f)
        {
            SceneManager.LoadScene(m_NameSceneMenu);
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<TouchPlayerEvent>(OnTouchPlayer);
    }
}
