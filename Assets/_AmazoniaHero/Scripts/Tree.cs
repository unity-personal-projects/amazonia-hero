﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;
using System;

public class Tree : MonoBehaviour
{
    private float m_LifePoint = 100f;
    private Coroutine m_TreeAttacked = null;
    [SerializeField]
    private GameObject[] m_Trees = new GameObject[10];
    [SerializeField]
    private GameObject m_TreeMeshParent = null;
    [SerializeField]
    private GameObject m_FlamesParticles = null;
    private ParticleSystem[] m_FlamesParticleSystems = null;
    [SerializeField]
    private AudioSource m_AudioSource = null;
    private void Awake()
    {
        EventManager.Instance.AddListener<TouchTreeEvent>(OnTouchTree);
        EventManager.Instance.AddListener<PlayerTouchTreeEvent>(OnPlayerTouchTree);
        GameObject g = Instantiate(m_Trees[UnityEngine.Random.Range(1, 10)]);
        m_FlamesParticleSystems = m_FlamesParticles.GetComponentsInChildren<ParticleSystem>(true);

        g.transform.SetParent(m_TreeMeshParent.transform);
        g.transform.localScale = Vector3.one;
        g.transform.localPosition = Vector3.zero;
        g.transform.rotation = Quaternion.AngleAxis(UnityEngine.Random.Range(0f, 360f), Vector3.up);
        for (int i = 0; i < m_FlamesParticleSystems.Length; i++)
        {
            m_FlamesParticleSystems[i].enableEmission = false;
        }
        m_AudioSource.mute = true;
    }

    private void OnPlayerTouchTree(PlayerTouchTreeEvent e)
    {
        if (e.tree.GetInstanceID() == gameObject.GetInstanceID())
        {
            if (m_TreeAttacked != null)
            {
                StopCoroutine(m_TreeAttacked);
            }
            m_TreeAttacked = null;
            m_AudioSource.mute = true;
            for (int i = 0; i < m_FlamesParticleSystems.Length; i++)
            {
                m_FlamesParticleSystems[i].enableEmission = false;
            }
        }
    }

    private void OnTouchTree(TouchTreeEvent e)
    {
        if (e.tree.GetInstanceID() == gameObject.GetInstanceID())
        {
            if (m_TreeAttacked == null)
            {
                for (int i = 0; i < m_FlamesParticleSystems.Length; i++)
                {
                    m_FlamesParticleSystems[i].enableEmission = true;
                }
                m_AudioSource.mute = false;
                m_TreeAttacked = StartCoroutine(TreeAttacked());
            }
        }
    }

    IEnumerator TreeAttacked()
    {
        while (m_LifePoint > 0f)
        {
            m_LifePoint -= .25f;
            yield return null;
        }
        EventManager.Instance.Raise(new TreeDestroyedEvent());
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<TouchTreeEvent>(OnTouchTree);
        EventManager.Instance.RemoveListener<PlayerTouchTreeEvent>(OnPlayerTouchTree);
    }

}
