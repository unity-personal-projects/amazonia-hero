﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class WaveCollider : MonoBehaviour
{
    [SerializeField]
    private GameObject m_EnemyPrefab = null;
    private GameObject m_Player = null;
    private bool m_HasBeenTriggered = false;
    private Coroutine m_WavesCoroutine = null;

    private void Awake()
    {
        m_Player = ((Player)FindObjectOfType(typeof(Player))).gameObject;
    }

    private void OnCollisionEnter(Collision other)
    {
        Debug.Log(other.gameObject.layer);
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            m_HasBeenTriggered = true;
        }
    }
    private void Update()
    {
        if (m_HasBeenTriggered && m_WavesCoroutine == null)
        {
            Debug.Log("Test");
            m_WavesCoroutine = StartCoroutine(StartWave(15));
        }
    }
    IEnumerator StartWave(int enemyAmount)
    {
        List<GameObject> enemies = new List<GameObject>();
        for (int i = 0; i < enemyAmount; i++)
        {
            GameObject enemy = Instantiate(m_EnemyPrefab);
            enemies.Add(enemy);
            float angle = UnityEngine.Random.Range(0f, 360f);
            float distX = transform.position.x + 15 * Mathf.Sin(angle);
            float distY = transform.position.y + 5f;
            float distZ = transform.position.z + 15 * Mathf.Cos(angle);
            enemy.transform.position = new Vector3(distX, distY, distZ);
            float randTime = UnityEngine.Random.Range(2f, 4f);
            yield return new WaitForSeconds(randTime);
        }

        // while (enemies.Count > 0)
        // {
        //     yield return null;
        // }
    }
}
