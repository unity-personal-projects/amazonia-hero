﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

#region GameManager Events
public class GameMenuEvent : SDD.Events.Event
{
}
public class GamePlayEvent : SDD.Events.Event
{
}
public class GamePauseEvent : SDD.Events.Event
{
}
public class GameResumeEvent : SDD.Events.Event
{
}
public class GameOverEvent : SDD.Events.Event
{
}
public class GameVictoryEvent : SDD.Events.Event
{
}
#endregion

#region Enemy Management Events

public class EnemyTouchedEvent : SDD.Events.Event
{
    public GameObject enemy;
}

public class TouchTreeEvent : SDD.Events.Event
{
    public GameObject enemy;
    public GameObject tree;
}

public class TouchPlayerEvent : SDD.Events.Event
{
    public GameObject enemy;
}
#endregion

public class TreeDestroyedEvent : SDD.Events.Event
{
}

public class PlayerTouchTreeEvent : SDD.Events.Event
{
    public GameObject tree;
}